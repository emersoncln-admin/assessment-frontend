## Instruções
- clonar o galho feature/001-front-end-test
- rodar o camando yarn para baixar as dependências
- rodar o comando yarn start ou npm start para iniciar o projeto


## Grunt
- Em um terminal paralelo
- rodar o camando grunt ou grunt watch para iniciar o grunt e compliar o sass
- o grunt espera por aletrações nos arquivos .scss e compila na pasta css

## Browser
- abrir no navegador localhost:8888/pages

## Informações
- Foram adiconadas a as telas de inicio, contato e a página de produtos segundo a lista.
- Foi utilizado o Sass
- Foi utilizado o grunt para automatizar as tarefas de compilação de estilos
- Foi utilizado o axios para consumir os dados da api
- Foi utlizado o bootstrap em auxilio do desenvolvimento
- Filtro removido da tela inicial
- Filtro adicionado somente na tela de catálogo
- Paginação simples